#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
using namespace std;

#include "format.cc"

#include <png.h>
#include <arpa/inet.h>

#define d50_xyz_icc_len 528
extern const uint8_t d50_xyz_icc[d50_xyz_icc_len];

void write_png_err(png_structp png, png_const_charp msg) {
  fprintf(stderr, "error: png: %s\n", msg);
  jmp_buf *jmp = (jmp_buf *) png_get_error_ptr(png);
  if (jmp) { longjmp(*jmp, 1); } else { abort(); }
}

bool write_png(FILE *out, int width, int height, png_bytepp rows) {
  jmp_buf jmp;
  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, &jmp, write_png_err, 0);
  if (! png) { return false; }
  png_infop info = png_create_info_struct(png);
  if (! info) { png_destroy_write_struct(&png, 0); return false; }
  if (setjmp(jmp)) { png_destroy_write_struct(&png, &info); return false; }
  png_init_io(png, out);
  png_set_compression_level(png, Z_BEST_COMPRESSION);
  png_set_IHDR
    ( png, info
    , width, height, 16
    , PNG_COLOR_TYPE_RGB
    , PNG_INTERLACE_ADAM7
    , PNG_COMPRESSION_TYPE_DEFAULT
    , PNG_FILTER_TYPE_DEFAULT
    );
  png_set_iCCP(png, info, (png_charp) "D50 XYZ", 0, (png_charp) d50_xyz_icc, d50_xyz_icc_len);
  png_write_info(png, info);
  png_write_image(png, rows);
  png_write_end(png, info);
  png_destroy_write_struct(&png, &info);
  return true;
}

int main(int argc, char **argv) {
  const char *stem = "prismatic";
  if (argc > 1) { stem = argv[1]; }
  float *f32 = 0;
  uint16_t *u16 = 0;
  uint16_t **row = 0;
  int allocated_width = -1;
  int allocated_height = -1;
  int width = 0;
  int height = 0;
  int frame = 0;
  // read image
  while (read_header_xyz(stdin, &width, &height)) {
    // allocate if necessary
    if (allocated_width != width && allocated_height != height) {
      if (f32) { free(f32); f32 = 0; }
      if (u16) { free(u16); u16 = 0; }
      if (row) { free(row); row = 0; }
      if (width > 0 && height > 0) {
        int bytes = width * height * 3 * sizeof(float);
        f32 = (float *) calloc(bytes, 1);
        if (! f32) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        bytes = width * height * 3 * sizeof(uint16_t);
        u16 = (uint16_t *) calloc(bytes, 1);
        if (! u16) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        bytes = height * sizeof(uint16_t *);
        row = (uint16_t **) calloc(bytes, 1);
        if (! row) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        for (int j = 0; j < height; ++j) {
          row[j] = u16 + j * width * 3;
        }
      } else {
        fprintf(stderr, "error: bad image size: %d %d\n", width, height);
        return 1;
      }
    } // allocate if necessary
    int count = width * height;
    if (! read_data_xyz(stdin, count, f32)) { break; }
    // transform colour space
    const float d50[3] = { 0.9642f, 1.0000f, 0.8249f };
    #pragma omp parallel for schedule(static)
    for (int k = 0; k < count; ++k) {
      f32[3*k+0] *= d50[0];
      f32[3*k+1] *= d50[1];
      f32[3*k+2] *= d50[2];
    }
    int count3 = count * 3;
    float ma = -1.0f / 0.0f;
    #pragma omp parallel for schedule(static) reduction(max: ma)
    for (int k = 0; k < count3; ++k) {
      ma = fmaxf(ma, f32[k]);
    }
    fprintf(stderr, "maximum at D50: %e\n", ma);
    // quantize to 16bit (FIXME TODO dither)
    float s = 65535.0f;// / ma;
    #pragma omp parallel for schedule(static)
    for (int k = 0; k < count3; ++k) {
      uint16_t u = fminf(fmaxf(s * f32[k], 0.0f), 65535.0f);
      u16[k] = htons(u);
    }
    // write image
    char fname[100];
    snprintf(fname, 100, "%s-%06d.png", stem, frame);
    FILE *out = fopen(fname, "wb");
    if (! out) { return 1; }
    write_png(out, width, height, (png_bytepp) row);
    fclose(out);
    frame++;
  }
  if (frame == 0) {
    fprintf(stderr, "warning: no images\n");
  }
  return 0;
}
