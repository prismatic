#!/bin/sh
cat "${1}/database/main/Al2O3/Malitson-o.yml" |
sed 's/\r//g' |
grep coefficients |
(
  read spam c1 c2 c3 c4 c5 c6 c7
  cat << EOF
#ifndef SAPPHIRE_H
#define SAPPHIRE_H 1

inline float sapphire(float wavelength) {
  float l2 = 1.0e-6f * wavelength * wavelength;
  return sqrt(1.0f + l2 *
    ( ${c2}f / (l2 - ${c3}f*${c3}f)
    + ${c4}f / (l2 - ${c5}f*${c5}f)
    + ${c6}f / (l2 - ${c7}f*${c7}f)
    ));
}

#endif
EOF
)
