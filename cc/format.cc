#include <cstdio>
using namespace std;

void write_header_intensity(FILE *f, int width, int height, float wavelength) {
  fprintf
    ( f
    , "prismatic\nintensity %e nm\nsize %d %d 1\nformat f32le\n\f"
    , wavelength
    , width
    , height
    );
}

void write_header_xyz(FILE *f, int width, int height) {
  fprintf
    ( f
    , "prismatic\nxyz\nsize %d %d 3\nformat f32le\n\f"
    , width
    , height
    );
}

void write_header_srgb(FILE *f, int width, int height) {
  fprintf
    ( f
    , "prismatic\nsrgb\nsize %d %d 3\nformat f32le\n\f"
    , width
    , height
    );
}

void write_data_intensity(FILE *f, int count, float *data) {
  fwrite(data, count * sizeof(*data), 1, f);
}

void write_data_xyz(FILE *f, int count, float *data) {
  fwrite(data, count * 3 * sizeof(*data), 1, f);
}

void write_data_srgb(FILE *f, int count, float *data) {
  fwrite(data, count * 3 * sizeof(*data), 1, f);
}

bool read_header_intensity(FILE *f, int *width, int *height, float *wavelength) {
  if (3 != fscanf
    ( f
    , "prismatic\nintensity %e nm\nsize %d %d 1\nformat f32le"
    , wavelength
    , width
    , height
    )) { return false; }
  if (fgetc(f) != '\n') { return false; }
  if (fgetc(f) != '\f') { return false; }
  return true;
}

bool read_header_xyz(FILE *f, int *width, int *height) {
  if (2 != fscanf
    ( f
    , "prismatic\nxyz\nsize %d %d 3\nformat f32le"
    , width
    , height
    )) { return false; }
  if (fgetc(f) != '\n') { return false; }
  if (fgetc(f) != '\f') { return false; }
  return true;
}

bool read_header_srgb(FILE *f, int *width, int *height) {
  if (2 != fscanf
    ( f
    , "prismatic\nsrgb\nsize %d %d 3\nformat f32le"
    , width
    , height
    )) { return false; }
  if (fgetc(f) != '\n') { return false; }
  if (fgetc(f) != '\f') { return false; }
  return true;
}

bool read_data_intensity(FILE *f, int count, float *data) {
  return 1 == fread(data, count * sizeof(*data), 1, f);
}

bool read_data_xyz(FILE *f, int count, float *data) {
  return 1 == fread(data, count * 3 * sizeof(*data), 1, f);
}

bool read_data_srgb(FILE *f, int count, float *data) {
  return 1 == fread(data, count * 3 * sizeof(*data), 1, f);
}
