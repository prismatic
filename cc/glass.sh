#!/bin/sh
cat "${1}/database/glass/schott/F2.yml" |
sed 's/\r//g' |
grep coefficients |
(
  read spam c1 c2 c3 c4 c5 c6 c7
  cat << EOF
#ifndef GLASS_H
#define GLASS_H 1

inline float glass_n(float wavelength) {
  float l2 = 1.0e-6f * wavelength * wavelength;
  return sqrt(1.0f + l2 *
    ( ${c2}f / (l2 - ${c3}f)
    + ${c4}f / (l2 - ${c5}f)
    + ${c6}f / (l2 - ${c7}f)
    ));
}

const float glass_lk[13][2] =
EOF
)
cat "${1}/database/glass/schott/F2.yml" |
sed 's/\r//g' |
tail -n+18 |
head -n 13 |
(
  sep="{"
  while read l k
  do
    echo "  ${sep} { 1000.0f * ${l}f, ${k}f }"
    sep=","
  done
)
cat <<EOF
  };

inline float glass_k(float wavelength) {
  for (int i0 = 0; i0 < 12; ++i0) {
    int i1 = i0 + 1;
    float l0 = glass_lk[i0][0];
    float l1 = glass_lk[i1][0];
    if (l0 <= wavelength && wavelength <= l1) {
      float f1 = (wavelength - l0) / (l1 - l0);
      float f0 = 1.0f - f1;
      float k0 = glass_lk[i0][1];
      float k1 = glass_lk[i1][1];
      return f0 * k0 + f1 * k1;
    }
  }
  return 1.0e-6f;
}

#endif
EOF
