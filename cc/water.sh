#!/bin/sh
cat "${1}/database/main/H2O/Hale.yml" |
tail -n+17 |
head -n 20 |
sed 's/\r//g' |
(
  cat <<EOF
#ifndef WATER_H
#define WATER_H 1

const float water_min   = 375;
const float water_step  =  25;
const int   water_count =  20;
const float water_nk[water_count][2] =
EOF
  sep="{"
  while read l n k
  do
    echo "  ${sep} { ${n}f, ${k}f }"
    sep=","
  done
  cat <<EOF
  };

inline float water(float l, int w) {
  float x  = (l - water_min) / water_step;
  int   i0 = floor(x);
  int   i1 = i0 + 1;
  float f1 = x - i0;
  float f0 = 1 - f1;
  if (i0 < 0) { return water_nk[0][w]; }
  if (i1 > water_count - 1) { return water_nk[water_count - 1][w]; }
  float y0 = water_nk[i0][w];
  float y1 = water_nk[i1][w];
  return f0 * y0 + f1 * y1;
}

#endif
EOF
)
