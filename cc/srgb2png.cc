#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
using namespace std;

#include "format.cc"

#include <png.h>

void write_png_err(png_structp png, png_const_charp msg) {
  fprintf(stderr, "error: png: %s\n", msg);
  jmp_buf *jmp = (jmp_buf *) png_get_error_ptr(png);
  if (jmp) { longjmp(*jmp, 1); } else { abort(); }
}

bool write_png(FILE *out, int width, int height, png_bytepp rows) {
  jmp_buf jmp;
  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, &jmp, write_png_err, 0);
  if (! png) { return false; }
  png_infop info = png_create_info_struct(png);
  if (! info) { png_destroy_write_struct(&png, 0); return false; }
  if (setjmp(jmp)) { png_destroy_write_struct(&png, &info); return false; }
  png_init_io(png, out);
  png_set_compression_level(png, PNG_Z_DEFAULT_COMPRESSION);
  png_set_IHDR
    ( png, info
    , width, height, 16
    , PNG_COLOR_TYPE_RGB
    , PNG_INTERLACE_ADAM7
    , PNG_COMPRESSION_TYPE_DEFAULT
    , PNG_FILTER_TYPE_DEFAULT
    );
  png_set_sRGB(png, info, PNG_sRGB_INTENT_ABSOLUTE);
  png_write_info(png, info);
  png_write_image(png, rows);
  png_write_end(png, info);
  png_destroy_write_struct(&png, &info);
  return true;
}

int main(int argc, char **argv) {
  const char *stem = "prismatic";
  if (argc > 1) { stem = argv[1]; }
  float *imagef = 0;
  uint16_t *image16 = 0;
  uint16_t **rows = 0;
  int allocated_width = -1;
  int allocated_height = -1;
  int width = 0;
  int height = 0;
  int frame = 0;
  while (read_header_srgb(stdin, &width, &height)) {
    // allocate if necessary
    if (allocated_width != width && allocated_height != height) {
      if (imagef) { free(imagef); imagef = 0; }
      if (image16) { free(image16); image16 = 0; }
      if (rows) { free(rows); rows = 0; }
      if (width > 0 && height > 0) {
        int bytes = width * height * 3 * sizeof(float);
        imagef = (float *) calloc(bytes, 1);
        if (! imagef) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        bytes = width * height * 3 * sizeof(uint16_t);
        image16 = (uint16_t *) calloc(bytes, 1);
        if (! image16) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        bytes = height * sizeof(uint16_t *);
        rows = (uint16_t **) calloc(bytes, 1);
        if (! rows) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        for (int j = 0; j < height; ++j) {
          rows[j] = image16 + j * width * 3;
        }
      } else {
        fprintf(stderr, "error: bad image size: %d %d\n", width, height);
        return 1;
      }
    } // allocate if necessary
    if (! read_data_srgb(stdin, width * height, imagef)) { return 1; }
    // quantize image
    int count = width * height * 3;
    #pragma omp parallel for schedule(static)
    for (int k = 0; k < count; ++k) {
      // FIXME TODO dither
      uint64_t v = fminf(fmaxf(65535.0f * imagef[k], 0), 65535);
      image16[k] = ((v & 0xff) << 8) | ((v >> 8) & 0xff);
    }
    // write image
    char fname[100];
    snprintf(fname, 100, "%s-%06d.png", stem, frame);
    FILE *out = fopen(fname, "wb");
    if (! out) { return 1; }
    write_png(out, width, height, (png_bytepp) rows);
    fclose(out);
    // advance frame counter
    frame++;
  }
  if (frame == 0) {
    fprintf(stderr, "warning: no images\n");
  }
  return 0;
}
