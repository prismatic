#include <cmath>
#include <cstdio>
#include <cstdlib>
using namespace std;

#include "format.cc"

inline bool xyz2srgb1
  ( const float &x, const float &y, const float &z
  ,       float &r,       float &g,       float &b
  ) {
  // white point
  float xw = x * 0.9505f;
  float yw = y;//1.0000f;
  float zw = z * 1.0890f;
  // linear rgb
  float rl =  3.2406f * xw + -1.5372f * yw + -0.4986f * zw;
  float gl = -0.9689f * xw +  1.8758f * yw +  0.0415f * zw;
  float bl =  0.0557f * xw + -0.2040f * yw +  1.0570f * zw;
  // check gammut
  bool  ok
    =  0.0f <= rl && rl <= 1.0f
    && 0.0f <= gl && gl <= 1.0f
    && 0.0f <= bl && bl <= 1.0f;
  r = rl;
  g = gl;
  b = bl;
  return ok;
}

inline void xyz2srgb2
  ( const float &x, const float &y, const float &z
  ,       float &r,       float &g,       float &b
  , const float &o, const float &s
  ) {
  // scale
  float rl = (x + o) * s;
  float gl = (y + o) * s;
  float bl = (z + o) * s;
  // clamp
  float rc = rl < 0.0f ? 0.0f : rl > 1.0f ? 1.0f : rl;
  float gc = gl < 0.0f ? 0.0f : gl > 1.0f ? 1.0f : gl;
  float bc = bl < 0.0f ? 0.0f : bl > 1.0f ? 1.0f : bl;
  // gamma
  float rg
    = rc <= 0.0031308f
    ? 12.92f * rc
    : 1.055f * pow(rc, 0.41666666f) - 0.055f;
  float gg
    = gc <= 0.0031308f
    ? 12.92f * gc
    : 1.055f * pow(gc, 0.41666666f) - 0.055f;
  float bg
    = bc <= 0.0031308f
    ? 12.92f * bc
    : 1.055f * pow(bc, 0.41666666f) - 0.055f;
  // output
  r = rg;
  g = gg;
  b = bg;
}

int cmp_float(const void *a, const void *b) {
  const float *p = (const float *) a;
  const float *q = (const float *) b;
  float x = *p;
  float y = *q;
  if (x < y) return -1;
  if (x > y) return  1;
  return 0;
}

int main() {
  const float lopercentile = 0.004;
  const float hipercentile = 0.999;
  float *image = 0;
  float *image2 = 0;
  int allocated_width = -1;
  int allocated_height = -1;
  int width = 0;
  int height = 0;
  int frame = 0;
  // read image
  while (read_header_xyz(stdin, &width, &height)) {
    // allocate if necessary
    if (allocated_width != width && allocated_height != height) {
      if (image) { free(image); image = 0; }
      if (image2) { free(image2); image2 = 0; }
      if (width > 0 && height > 0) {
        int bytes = width * height * 3 * sizeof(float);
        image = (float *) calloc(bytes, 1);
        if (! image) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        image2 = (float *) calloc(bytes, 1);
        if (! image2) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
      } else {
        fprintf(stderr, "error: bad image size: %d %d\n", width, height);
        return 1;
      }
    } // allocate if necessary
    int count = width * height;
    if (! read_data_xyz(stdin, count, image)) { break; }
    // transform colour space
    int out_of_gamut = 0;
    #pragma omp parallel for schedule(static)
    for (int k = 0; k < count; ++k) {
      float x = image[3*k+0];
      float y = image[3*k+1];
      float z = image[3*k+2];
      float r, g, b;
      bool ok = xyz2srgb1(x, y, z, r, g, b);
      image2[3*k+0] = image[3*k+0] = r;
      image2[3*k+1] = image[3*k+1] = g;
      image2[3*k+2] = image[3*k+2] = b;
      if (! ok) {
        #pragma omp atomic
        out_of_gamut++;
      }
    }
    // diagnostics
    if (out_of_gamut) {
      fprintf
        ( stderr
        , "warning: %f%% outside sRGB gamut\n"
        , out_of_gamut * 100.0 / count
        );
    }
    qsort(image2, count * 3, sizeof(float), cmp_float);
    float lo = image2[int(ceil(count * 3 * lopercentile))    ];
    float hi = image2[int(ceil(count * 3 * hipercentile)) - 1];
    lo = fminf(lo, 0.0f);
    fprintf
      ( stderr
      , "info: rescaling linear RGB from (%f, %f) to (0, 1);\n"
      , lo, hi
      );
    float o = -lo;
    float s = 1.0f / (hi - lo);
    #pragma omp parallel for schedule(static)
    for (int k = 0; k < count; ++k) {
      float x = image[3*k+0];
      float y = image[3*k+1];
      float z = image[3*k+2];
      float r, g, b;
      xyz2srgb2(x, y, z, r, g, b, o, s);
      image[3*k+0] = r;
      image[3*k+1] = g;
      image[3*k+2] = b;
    }
    // write image
    write_header_srgb(stdout, width, height);
    write_data_srgb(stdout, count, image);
    fflush(stdout);
    frame++;
  }
  if (frame == 0) {
    fprintf(stderr, "warning: no images\n");
  }
  return 0;
}
