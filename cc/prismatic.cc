// feature test macro requirement for glibc clock_gettime()
#define _POSIX_C_SOURCE 199309L
#include <time.h>

#include "lin2012xyz2e_1_7sf.h"
#include <glm/glm.hpp>
using namespace glm;
using namespace std;
#include "water.h"
#include "glass.h"
#include "quartz.h"
/*
#include "sapphire.h"
*/
#include "format.cc"

#ifndef IMAGE_SIZE
#define IMAGE_SIZE 5
#endif

#ifndef DEPTH_MAX
#define DEPTH_MAX 4
#endif

// { config {
const int   image_size = IMAGE_SIZE;
const int   image_width  = 22 << image_size;
const int   image_height =  7 << image_size;
const int   frames = 1; // 375;
const int   fps = 25;
const float animation_duration = 12.0f;
const int   animation_spin = -1;
const int   animation_push =  1;
/*
const float view_XY = 30.0f;
const float view_XZ =  0.0f;
const float view_XW = 30.0f;
const float view_YZ =  0.0f;
const float view_YW = 30.0f;
const float view_ZW =  0.0f;
*/
const float sphere_size = 2.0f/3.0f;
/*
const float light_size = 0.1f;
*/
const int   depth_max = DEPTH_MAX;

const int   wavelength_start = 556; // peak brightness
const int   wavelength_count = 441; // number of wavelengths to trace
const int   wavelength_wrap = 441;
const int   wavelength_step = 169;  // important: gcd(wrap, step) = 1
const int   wavelength_lo = wavelength_min;
const int   wavelength_hi = wavelength_lo + wavelength_wrap;
// } config }

const int   spheres = 24;
const int   lights = 1;

// advance rays away from surface intersection
const float acne = 0.0001f;
const float cacne = cos(acne);
const float sacne = sin(acne);

const float pi = 3.141592653589793f;


typedef unsigned char uint8;

/*
// <https://commons.wikimedia.org/wiki/File:Lambert_cylindrical_equal-area_projection_SW.jpg>
const int   earth_width  = 2044;
const int   earth_height =  650;
uint8 earth_srgb[earth_height][earth_width][3];

float earth_phase = 0.0f;
*/
float earth(vec4 ray, float wavelength) {
  (void) wavelength;
  vec3 dir(ray);
  {
    float t = pi / 3.0f;
    float c = cos(t);
    float s = sin(t);
    float z = 0.0f;
    float i = 1.0f;
    dir = mat3(c, s, z, -s, c, z, z, z, i) * dir;
  }
  dir = normalize(dir);
  float x = atan(dir[0], dir[2]) / pi;
  float y = dir[1];
  y = asin(y) / pi;
  bool  k = (fmod(12.0f * (x + 1.0f), 1.0f) > 0.5f)
         != (fmod(12.0f * (y + 1.0f), 1.0f) > 0.5f);
  return k ? 1.0f : 1.0f/64.0f;
/*
  int   i = (x / 2.0f + 0.5f + earth_phase) * earth_width;
  int   j = (y / 2.0f + 0.5f) * earth_height;
  i = (i + earth_width) % earth_width;
  if (j < 0) { j = 0; }
  if (j >= earth_height) { j = earth_height - 1; }
  auto e = earth_srgb[j][i];
  // <https://en.wikipedia.org/wiki/SRGB#The_reverse_transformation>
  vec3 rgb(0.0f);
  for (int c = 0; c < 3; ++c) {
    rgb[c] = e[c] / 255.0f;
    if (rgb[c] <= 0.04045f) {
      rgb[c] /= 12.92;
    } else {
      rgb[c] = pow((rgb[c] + 0.055f) / 1.055f, 2.4f);
    }
  }
  vec3 exyz(transpose(mat3
    ( 0.4124f, 0.3576f, 0.1805f
    , 0.2126f, 0.7152f, 0.0722f
    , 0.0193f, 0.1192f, 0.9505f
    )) * rgb);
  auto stimulus = tristimulus_curve[int(wavelength) - wavelength_min];
  vec3 xyz(stimulus[0], stimulus[1], stimulus[2]);
  return dot(xyz, exyz);
*/
}

/*
vec4  image_xyza[image_height][image_width];
uint8 image_srgb[image_height][image_width][3];
*/
float image_intensity[image_height][image_width];

struct material {
  bool  light;
  float index;
  float emission;
  float transmission;
};

enum material_t
  { material_vacuum = 0
  , material_water
  , material_quartz
  , material_glass
  , material_white
  , material_red
  , material_yellow
  , material_green
  , material_blue
  , material_earth
  , material_count
  };

material materials[material_count];


struct sphere {
  vec4       center;
  float      radius;
  float      sradius;
  float      cradius;
  material_t material;
};

sphere scene[spheres + lights];

inline vec4 cross(const vec4 &a, const vec4 &b, const vec4 &c) {
  // <http://www.gamedev.net/topic/456301-cross-product-vector-4d/#entry4011062>
  float ax = a[0];
  float ay = a[1];
  float az = a[2];
  float aw = a[3];
  float bx = b[0];
  float by = b[1];
  float bz = b[2];
  float bw = b[3];
  float cx = c[0];
  float cy = c[1];
  float cz = c[2];
  float cw = c[3];
  float dx =  ay*(bz*cw - cz*bw) - az*(by*cw - cy*bw) + aw*(by*cz - cy*bz);
  float dy = -ax*(bz*cw - cz*bw) + az*(bx*cw - cx*bw) - aw*(bx*cz - cx*bz);
  float dz =  ax*(by*cw - cy*bw) - ay*(bx*cw - cx*bw) + aw*(bx*cy - cx*by);
  float dw = -ax*(by*cz - cy*bz) + ay*(bx*cz - cx*bz) - az*(bx*cy - cx*by);
  return vec4(dx, dy, dz, dw);
}

inline mat4 transport(const vec4 &target) {
  vec4 a(1.0f, 0.0f, 0.0f, 0.0f);
  vec4 b(0.0f, 1.0f, 0.0f, 0.0f);
  vec4 c(0.0f, 0.0f, 1.0f, 0.0f);
  vec4 d(target);
  a = normalize(cross(b, c, d));
  b = normalize(cross(c, d, a));
  c = normalize(cross(d, a, b));
  return mat4(a, b, c, d);
}

inline mat4 lookAt(const vec4 &target, const vec4 &eye) {
  return transpose(transport(eye)) * transport(target);
}


inline float dot(const vec4 &a, const vec4 &b) {
  float ax = a[0];
  float ay = a[1];
  float az = a[2];
  float aw = a[3];
  float bx = b[0];
  float by = b[1];
  float bz = b[2];
  float bw = b[3];
  return ax * bx + ay * by + az * bz + aw * bw;
}

inline vec4 reflect(const vec4 &I, const vec4 &N, float dotNI) {
  return I - (2.0f * dotNI) * N;
}

inline vec4 refract(const vec4 &I, const vec4 &N, float eta, float dotNI) {
  float k = 1.0f - eta * eta * (1.0f - dotNI * dotNI);
  if (k < 0.0f) {
    return vec4(0.0f);
  } else {
    return eta * I - (eta * dotNI + sqrt(k)) * N;
  }
}


// Fresnel equations
// I = incident ray
// N = surface normal
// eta = n1 / n2
// result.x = reflected power
// result.y = transmitted power
inline vec2 fresnel(float dotNI, float eta) {
  float c1 = -dotNI;
  float c2 = 1.0f - (1.0f - c1 * c1) * (eta * eta);
  if (c2 < 0.0f) {
    // total internal reflection
    return vec2(1.0f, 0.0f);
  }
  c2 = sqrt(c2);
  float Rs = (eta * c1 - c2) / (eta * c1 + c2);
  float Rp = (eta * c2 - c1) / (eta * c2 + c1);
  float R = 0.5f * (Rs * Rs + Rp * Rp);
  float T = 1.0f - R;
  return vec2(R, T);
}


// spherical tangent space
// points and directions are unit length
// directions from the pole are equivalent to points on the equator
// result is direction towards "to" orthogonal to "from"
inline vec4 tangent(const vec4 &to, const vec4 &from, float c, float s) {
  return (to - c * from) * (1.0f / s);
}

inline vec4 tangent(const vec4 &to, const vec4 &from, float dotTF) {
  return tangent(to, from, dotTF, sqrt(1.0f - dotTF * dotTF));
}

inline vec4 tangent(const vec4 &to, const vec4 &from) {
  return tangent(to, from, dot(to, from));
}


mat4 initialize_view() {
/*
  float cXY = cos(radians(view_XY));
  float sXY = sin(radians(view_XY));
  float cXZ = cos(radians(view_XZ));
  float sXZ = sin(radians(view_XZ));
  float cXW = cos(radians(view_XW));
  float sXW = sin(radians(view_XW));
  float cYZ = cos(radians(view_YZ));
  float sYZ = sin(radians(view_YZ));
  float cYW = cos(radians(view_YW));
  float sYW = sin(radians(view_YW));
  float cZW = cos(radians(view_ZW));
  float sZW = sin(radians(view_ZW));
  mat4 mXYZW = mat4
    (  cXY, sXY, 0.0f, 0.0f
    , -sXY, cXY, 0.0f, 0.0f
    , 0.0f, 0.0f,  cZW, sZW
    , 0.0f, 0.0f, -sZW, cZW
    );
  mat4 mXZYW = mat4
    (  cXZ, 0.0f, sXZ, 0.0f
    , 0.0f,  cYW, 0.0f, sYW
    , -sXZ, 0.0f, cXZ, 0.0f
    , 0.0f, -sYW, 0.0f, cYW
    );
  mat4 mXWYZ = mat4
    (  cXW, 0.0f, 0.0f, sXW
    , 0.0f,  cYZ, sYZ, 0.0f
    , 0.0f, -sYZ, cYZ, 0.0f
    , -sXW, 0.0f, 0.0f, cXW
    );
  return mXYZW * mXZYW * mXWYZ;
  float p = sqrt(0.5f);
  float z = 0.0f;
  return lookAt(vec4(z,z,p,p), vec4(z,p,z,p));
*/
  float t = radians(160.0f);
  float c = cos(t);
  float s = sin(t);
  float z = 0.0f;
  float i = 1.0f;
  mat4  m(c,s,z,z,-s,c,z,z,z,z,i,z,z,z,z,i);
  return m * lookAt(vec4(0.5,0.5,0.5,0.5), normalize(vec4(0.1,0.5,-0.1,1.0)));
}

mat4 initialize_animation(float time) {
  float t = 2.0f * pi * time / animation_duration;
  float cSpin = cos(float(animation_spin) * t);
  float sSpin = sin(float(animation_spin) * t);
  float cPush = cos(float(animation_push) * t);
  float sPush = sin(float(animation_push) * t);
  return mat4
    (  cSpin, sSpin, 0.0f, 0.0f
    , -sSpin, cSpin, 0.0f, 0.0f
    , 0.0f, 0.0f,  cPush, sPush
    , 0.0f, 0.0f, -sPush, cPush
    );
}


vec4 lambert_cylindrical_equal_area_projection(float x, float y) {
  float sx = -sin(x);
  float cx = -cos(x);
  float sy = y;
  float cy = sqrt(1.0f - sy * sy);
  return vec4(cy * sx, sy, cy * cx, 0.0f);
}

/*
float simple_index(float wavelength) {
  return 2.5f - 0.75f
    * (wavelength     - wavelength_min)
    / (wavelength_max - wavelength_min);
}


float simple_spectrum(float wavelength_peak, float quality, float wavelength) {
  float delta = quality
    * (wavelength     - wavelength_peak)
    / (wavelength_max - wavelength_min);    
  return 1.0f / (1.0f + delta * delta);
}
*/

void initialize_materials(float wavelength) {
  materials[material_vacuum].light        = false;
  materials[material_vacuum].index        = 1.0f;
  materials[material_vacuum].emission     = 0.0f;
  materials[material_vacuum].transmission = 1.0f;
  materials[material_water ].light        = false;
  materials[material_water ].index        = water(wavelength, 0);
  materials[material_water ].emission     = 0.0f;
  materials[material_water ].transmission =
      exp(-4.0f * pi * water(wavelength, 1) / (wavelength * 1.0e-9f));
  materials[material_glass ].light        = false;
  materials[material_glass ].index        = glass_n(wavelength);
  materials[material_glass ].emission     = 0.0f;
  materials[material_glass ].transmission =
      exp(-4.0f * pi * glass_k(wavelength) / (wavelength * 1.0e-9f));
  materials[material_quartz].light        = false;
  materials[material_quartz].index        = quartz(wavelength, 0);
  materials[material_quartz].emission     = 0.0f;
  materials[material_quartz].transmission =
      exp(-4.0f * pi * quartz(wavelength, 1) / (wavelength * 1.0e-9f));
/*
  materials[material_white ].light        = true;
  materials[material_white ].index        = 1.0f;
  materials[material_white ].emission     = 8.0f;
  materials[material_white ].transmission = 1.0f;
  materials[material_red   ].light        = true;
  materials[material_red   ].index        = 1.0f;
  materials[material_red   ].emission     = simple_spectrum(675, 2, wavelength);
  materials[material_red   ].transmission = 1.0f;
  materials[material_yellow].light        = true;
  materials[material_yellow].index        = 1.0f;
  materials[material_yellow].emission     = simple_spectrum(575, 2, wavelength);
  materials[material_yellow].transmission = 1.0f;
  materials[material_green ].light        = true;
  materials[material_green ].index        = 1.0f;
  materials[material_green ].emission     = simple_spectrum(525, 2, wavelength);
  materials[material_green ].transmission = 1.0f;
  materials[material_blue  ].light        = true;
  materials[material_blue  ].index        = 1.0f;
  materials[material_blue  ].emission     = simple_spectrum(425, 2, wavelength);
  materials[material_blue  ].transmission = 1.0f;
*/
  materials[material_earth ].light        = true;
  materials[material_earth ].index        = 1.0f;
  materials[material_earth ].emission     = 0.0f; // magic
  materials[material_earth ].transmission = 0.0f;
}


void initialize_scene(mat4 animation, mat4 view) {
  mat4 rotation(animation * view);
  // at pi/6 the spheres just touch, so sphere_size is in 0..1
  float sphere_radius  = sphere_size * pi / 6.0f;
  float sphere_sradius = sin(sphere_radius);
  float sphere_cradius = cos(sphere_radius);
/*
  // the light must be smaller than the sphere, so light size is in 0..1
  float light_radius = light_size * sphere_radius;
  float light_sradius = sin(light_radius);
  float light_cradius = cos(light_radius);
*/
  int k = 0;
  {
/*
    float p = sqrt(0.5f);
    float z = 0.0f;
*/
    vec4  q = animation
/*
            * inverse(lookAt(vec4(z,z,p,p), vec4(z,p,z,p)))
*/
            * vec4(0.0f, 0.0f, 0.0f, 1.0f);
    scene[k].center   = -q;
    scene[k].radius   = pi / 2.0f;
    scene[k].sradius  = 1.0f;
    scene[k].cradius  = 0.0f;
    scene[k].material = material_earth;
    k = k + 1;
  }
  // 24-cell has the vertices of a cube and a cross
  // three sets of eight vertices, each set in its own colour
  // cube
  for (int sx = -1; sx <= 1; sx += 2) {
  for (int sy = -1; sy <= 1; sy += 2) {
  for (int sz = -1; sz <= 1; sz += 2) {
  for (int sw = -1; sw <= 1; sw += 2) {
    bool odd = ((sx + sy + sz + sw) & 2) == 2;
    vec4 p  = rotation
            * (0.5f * vec4(float(sx), float(sy), float(sz), float(sw)));
    scene[k].center   = p;
    scene[k].radius   = sphere_radius;
    scene[k].sradius  = sphere_sradius;
    scene[k].cradius  = sphere_cradius;
    scene[k].material = odd ? material_quartz : material_glass;
/*
    int l = k + spheres;
    scene[l].center   = p;
    scene[l].radius   = light_radius;
    scene[l].sradius  = light_sradius;
    scene[l].cradius  = light_cradius;
    scene[l].material = material_white; // material_earth;
*/
    k = k + 1;
  }}}}
  // cross
  for (int sd = 0; sd < 4; sd += 1) {
  for (int sv = -1; sv <= 1; sv += 2) {
    vec4 p = vec4(0.0f);
    p[sd] = float(sv);
    p = rotation * p;
    scene[k].center   = p;
    scene[k].radius   = sphere_radius;
    scene[k].sradius  = sphere_sradius;
    scene[k].cradius  = sphere_cradius;
    scene[k].material = material_water;
/*
    int l = k + spheres;
    scene[l].center   = p;
    scene[l].radius   = light_radius;
    scene[l].sradius  = light_sradius;
    scene[l].cradius  = light_cradius;
    scene[l].material = material_white; // material_earth;
*/
    k = k + 1;
  }}
}


material_t material_at(vec4 p) {
  float cdistance = -2.0;
  material_t material = material_vacuum;
  for (int k = 0; k < spheres + lights; ++k) {
    float cd = dot(p, scene[k].center);
    if (cd > scene[k].cradius && cd > cdistance) {
      material = scene[k].material;
    }
  }
  return material;
}

/*
void image_clear_xyza() {
  #pragma omp parallel for schedule(static)
  for (int image_j = 0; image_j < image_height; ++image_j) {
    for (int image_i = 0; image_i < image_width; ++image_i) {
      image_xyza[image_j][image_i] = vec4(0.0f);
    }
  }
}

inline float srgb(float c0) {
  float c = clamp(c0, 0.0f, 1.0f);
  if (c <= 0.0031308f) {
    return 12.92f * c;
  } else {
    return 1.055f * pow(c, 1.0f / 2.4f) - 0.055f;
  }
}

inline vec3 xyz_to_srgb(const vec3 &xyz) {
  vec3 white(0.9505f, 1.0000f, 1.0890f);
  vec3 rgb = transpose(mat3
    (  3.2406f, -1.5372f, -0.4986f
    , -0.9689f,  1.8758f,  0.0415f
    ,  0.0557f, -0.2040f,  1.0570f
    )) * (xyz * white);
  return vec3(srgb(rgb.r), srgb(rgb.g), srgb(rgb.b));
}

void image_xyza_to_srgb() {
  #pragma omp parallel for schedule(static)
  for (int image_j = 0; image_j < image_height; ++image_j) {
    for (int image_i = 0; image_i < image_width; ++image_i) {
      vec4 xyza(image_xyza[image_j][image_i]);
      vec3 srgb(xyz_to_srgb(vec3(xyza) / xyza.a));
      for (int image_c = 0; image_c < 3; ++image_c) {
        image_srgb[image_j][image_i][image_c] =
            uint8(clamp(255.0f * srgb[image_c], 0.0f, 255.0f));
      }
    }
  }
}

void image_save_ppm() {
  printf("P6\n%d %d\n255\n", image_width, image_height);
  fwrite(&image_srgb[0][0][0], image_width * image_height * 3, 1, stdout);
}
*/

// trace rays
float trace
  ( int depth
  , float wavelength
  , material_t material
  , const vec4 &eye
  , const vec4 &ray
  ) {
  if (depth <= 0) {
    return 0.0f;
  }
  // initialize results
  bool       hit          = false;           // did the ray hit anything
  float      hitcdistance = -1.0f / 0.0f;    // keep only the nearest hit
  float      hitsdistance = 0.0f;
  vec4       hitpos       = vec4(0.0f);      // ray-surface intersection point
  vec4       hitnormal    = vec4(0.0f);      // surface normal at intersection
  material_t hitmaterial  = material_vacuum; // material beyond the surface
  int        hitk = -1;
  // check every object in the scene
  for (int k = 0; k < spheres + lights; ++k) {
    vec4  center  = scene[k].center;
    // r = sphere radius
    float sr      = scene[k].sradius;
    float cr      = scene[k].cradius;
    // d = distance from eye to center
    float cd      = dot(eye, center);
    float sd      = sqrt(clamp(1.0f - cd * cd, 0.0f, 1.0f));
    // early rejection test
    float cdr = cd * cr + sd * sr;
    if (cdr < hitcdistance) {
      // nearest point on sphere = d - r > nearest hit
      continue;
    }

    // theta = angle subtended by the sphere radius
    float stheta  = sr / sd;
    // phi = angle between ray and sphere center
    vec4  tcenter = tangent(center, eye, cd, sd);
    float cphi    = dot(ray, tcenter);
    if (length(center + eye) < acne) { cphi = 1.0f; }
    if (length(center - eye) < acne) { cphi = 1.0f; }

    if (stheta > 1.0f) {
      // find the distance to the intersection
      if (cd > cr) {
        // d < r
        // sphere surrounds eye
        // ray intersects from the inside
        float a = cr;
        float b = cd;
        float c = sd * cphi;
        // tan (h/2) = ... / ...
        // cos h = (1 - tan^2(h/2)) / (1 + tan^2(h/2))
        // sin h = 2 tan(h/2) / (1 + tan^2(h/2))
        // ... c + sqrt ... because the other intersection is behind
        float th2 = (c + sqrt(c * c + b * b - a * a)) / (a + b);
        float t2h2 = th2 * th2;
        float t2h2p1 = 1.0f + t2h2;
        float ch = (1.0f - t2h2) / t2h2p1;
        // check if it's closer
        if (ch > hitcdistance) {
          float sh = 2.0f * th2 / t2h2p1;
          // update results with new details
          hit          = true;
          hitcdistance = ch;
          hitsdistance = sh;
          // eye and ray are orthogonal unit vectors
          hitpos       = ch * eye + sh * ray;
          // surface normal points towards sphere center
          hitnormal    =  tangent(center, hitpos, cr, sr);
          // outside the sphere is vacuum
          hitmaterial  = material_vacuum;
          hitk         = k;
        }
      } else {
        // sphere surrounds -eye
        // ray intersects from the outside
        float a = cr;
        float b = cd;
        float c = sd * cphi;
        // ... c - sqrt ... because the other intersection is further
        float th2 = (c - sqrt(c * c + b * b - a * a)) / (a + b);
        float t2h2 = th2 * th2;
        float t2h2p1 = 1.0f + t2h2;
        float ch = (1.0f - t2h2) / t2h2p1;
        // check if it's closer
        if (ch > hitcdistance) {
          float sh = 2.0f * th2 / t2h2p1;
          // update results with new details
          hit          = true;
          hitcdistance = ch;
          hitsdistance = sh;
          // eye and ray are orthogonal unit vectors
          hitpos       = ch * eye + sh * ray;
          // surface normal points away from sphere center
          hitnormal    = -tangent(center, hitpos, cr, sr);
          // inside the sphere is the sphere's material
          hitmaterial  = scene[k].material;
          hitk         = k;
/*
          if (depth == depth_max && k == 0) {
            fprintf(stderr, "\t\t%f\t%f\n", ch, sh);
          }
*/
        }
      }

    } else {
      // d > r
      // sphere is disjoint from eye, ray might intersect from outside
      float ctheta2 = 1.0f - stheta * stheta;
      if (cphi >= 0.0f && cphi * cphi > ctheta2) {
        // phi < theta
        // ray does intersect
        // find the distance to the intersection
        float a = cr;
        float b = cd;
        float c = sd * cphi;
        // ... c - sqrt ... because the other intersection is further
        float th2 = (c - sqrt(c * c + b * b - a * a)) / (a + b);
        float t2h2 = th2 * th2;
        float t2h2p1 = 1.0f + t2h2;
        float ch = (1.0f - t2h2) / t2h2p1;
        // check if it's closer
        if (ch > hitcdistance) {
          float sh = 2.0f * th2 / t2h2p1;
          // update results with new details
          hit          = true;
          hitcdistance = ch;
          hitsdistance = sh;
          // eye and ray are orthogonal unit vectors
          hitpos       = ch * eye + sh * ray;
          // surface normal points away from sphere center
          hitnormal    = -tangent(center, hitpos, cr, sr);
          // inside the sphere is the sphere's material
          hitmaterial  = scene[k].material;
          hitk         = k;
        }
      }
    }
  } // for each object in scene

  float intensity = 0.0f;
  if (hit) {
    // attenuation by the near-side material through which the ray travelled
    // FIXME acos()
    float transmission =
        pow(materials[material].transmission, 2.0 * acos(hitcdistance));
    // FIXME non-physical fading with distance
    float fade = 1.0f;//0.5f * (1.0f + hitcdistance);
    // contribution from this ray
    float factor = transmission * fade;
    // incident ray
    vec4 incident = hitcdistance * ray - hitsdistance * eye;
    // Lambert diffuse
    float dotNI = dot(hitnormal, incident);
    float emit = clamp(-dotNI, 0.0f, 1.0f);
    // accumulate this ray
    float emission = 0.0f;
    if (hitmaterial == material_earth) { // magic
      vec4 middle =
          hitpos - dot(hitpos, scene[hitk].center) * scene[hitk].center;
      emission = earth(middle, wavelength);
    } else {
      emission = materials[hitmaterial].emission;
    }
    intensity += factor * emit * emission;
    if ((! materials[hitmaterial].light) && depth > 1 && factor > 0.0f) {
      float eta = materials[material].index / materials[hitmaterial].index;
      // Fresnel equations for intensity of reflection and refraction
      vec2 f = fresnel(dotNI, eta);
      if (f.x > 0.0f) {
        // reflected ray travels through the near side material
        // incident and hitnormal are both tangent to hitpos,
        //    so too will be the reflected ray
        // incident and hitnormal are both unit,
        //    so too will be the reflected ray
        vec4 ray2 = reflect(incident, hitnormal, dotNI);
        // rotate away from surface
        vec4 eye2 = hitpos * cacne + sacne * ray2;
        ray2      = ray2   * cacne - sacne * hitpos;
        // re-normalize to avoid acne at high depths
        eye2 = normalize(eye2);
        ray2 = normalize(ray2);
        // trace reflected ray
        intensity += factor * f.x *
            trace(depth - 1, wavelength,    material, eye2, ray2);
      }
      if (f.y > 0.0f) {
        // refracted ray travels through the far side material
        // incident and hitnormal are both tangent to hitpos,
        //     so too will be the refracted ray
        // incident and hitnormal are both unit,
        //     so too will be the refracted ray
        vec4 ray2 = refract(incident, hitnormal, eta, dotNI);
        // rotate away from surface
        vec4 eye2 = hitpos * cacne + sacne * ray2;
        ray2      = ray2   * cacne - sacne * hitpos;
        // re-normalize to avoid acne at high depths
        eye2 = normalize(eye2);
        ray2 = normalize(ray2);
        // trace refracted ray
        intensity += factor * f.y *
            trace(depth - 1, wavelength, hitmaterial, eye2, ray2);
      }
    }
  }

  return intensity;
}

int main(int argc, char **argv) {
  const char *stem = "prismatic";
  if (argc > 1) { stem = argv[1]; }
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  time_t then = now.tv_sec;
/*
  fprintf(stderr, "# nm water glass quartz\n");
  for (int wavelength = wavelength_min; wavelength <= wavelength_max; wavelength += wavelength_step) {
    fprintf(stderr, "%d %f %f %f\n", wavelength, water(wavelength, 0), glass_n(wavelength), quartz(wavelength, 0));
  }
*/
/*
  FILE *e = fopen("earth.ppm", "rb");
  if (e) {
    fseek(e, 16, SEEK_SET);
    fread(earth_srgb, 3 * earth_width * earth_height, 1, e);
    fclose(e);
  } else {
    return 1;
  }
*/
  vec4 eye(0.0f, 0.0f, 0.0f, 1.0f);
  mat4 view(initialize_view());
  for (int frame = 0; frame < frames; ++frame) {
    fprintf(stderr, "\r%7d ", frame);
    float time = frame / float(fps);
/*
    earth_phase = time / 6.0f;
*/
    mat4 animation(initialize_animation(time));
    initialize_scene(animation, view);
    material_t material(material_at(eye));
/*
    image_clear_xyza();
*/
    char fname[100];
    snprintf(fname, 100, "%s-%06d.prism", stem, frame);
    FILE *out = fopen(fname, "wb");
    for (int w = 0; w < wavelength_count; ++ w) {
      int wavelength
        = wavelength_lo
        + ( (wavelength_start - wavelength_lo + w * wavelength_step)
          % wavelength_wrap);
/*
      auto stimulus = tristimulus_curve[wavelength - wavelength_min];
      vec3 xyz(stimulus[0], stimulus[1], stimulus[2]);
*/
      initialize_materials(wavelength);
      write_header_intensity(out, image_width, image_height, wavelength);
      int progress = 0;
      #pragma omp parallel for schedule(dynamic, 1)
      for (int image_j = 0; image_j < image_height; ++image_j) {
        #pragma omp critical
        {
          clock_gettime(CLOCK_MONOTONIC, &now);
          if (now.tv_sec > then) {
            then = now.tv_sec;
            float percent
              = ( frame    + 0.0f
              + ( w        + 0.0f
              + ( progress + 0.0f
              ) / image_height
              ) / wavelength_count
              ) / frames;
            fprintf
              ( stderr
              , "\r%6d/%6d : %3d/%3d (%3dnm) : %5d/%5d : %7.3f%%"
              , frame + 1, frames
              , w + 1, wavelength_count, wavelength
              , progress + 1, image_height
              , 100 * percent
              );
          }
        }
        float screen_y = 2.0f * ((image_j + 0.5f) / image_height - 0.5f);
        for (int image_i = 0; image_i < image_width; ++image_i) {
          float screen_x = pi * 2.0f * ((image_i + 0.5f) / image_width - 0.5f);
          vec4 ray(lambert_cylindrical_equal_area_projection(screen_x, screen_y));
          float intensity(trace(depth_max, wavelength, material, eye, ray));
          if (0.0f <= intensity && intensity < 1.0f / 0.0f) {
            image_intensity[image_j][image_i] = intensity;
/*
            intensity *= 3.0f;
            image_xyza[image_j][image_i] += vec4(intensity * xyz, 1.0);
*/
          } else {
            image_intensity[image_j][image_i] = -1.0f;
          }
        }
        #pragma omp atomic
        progress++;
      }
      write_data_intensity(out, image_width * image_height, &image_intensity[0][0]);
      fflush(out);
    }
    fclose(out);
/*
    image_xyza_to_srgb();
    image_save_ppm();
*/
  }
  fprintf(stderr, "\r");
  return 0;
}
