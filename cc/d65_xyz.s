.section .rodata
.global d65_xyz_icc
d65_xyz_icc:
.incbin "D65_XYZ.icc"
.global d65_xyz_icc_end
d65_xyz_icc_end:
.global d65_xyz_icc_len
.set d65_xyz_icc_len, d65_xyz_icc_end - d65_xyz_icc
