#include <cmath>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
using namespace std;

#include <png.h>
#include <arpa/inet.h>

inline bool xyz2srgb1
  ( const float &x, const float &y, const float &z
  ,       float &r,       float &g,       float &b
  ) {
  // white point
  float xw = x * 0.9505f;
  float yw = y;//1.0000f;
  float zw = z * 1.0890f;
  // linear rgb
  float rl =  3.2406f * xw + -1.5372f * yw + -0.4986f * zw;
  float gl = -0.9689f * xw +  1.8758f * yw +  0.0415f * zw;
  float bl =  0.0557f * xw + -0.2040f * yw +  1.0570f * zw;
  // check gammut
  bool  ok
    =  0.0f <= rl && rl <= 1.0f
    && 0.0f <= gl && gl <= 1.0f
    && 0.0f <= bl && bl <= 1.0f;
  r = rl;
  g = gl;
  b = bl;
  return ok;
}

inline void xyz2srgb2
  ( const float &x, const float &y, const float &z
  ,       float &r,       float &g,       float &b
  , const float &o, const float &s
  ) {
  // scale
  float rl = (x + o) * s;
  float gl = (y + o) * s;
  float bl = (z + o) * s;
  // clamp
  float rc = rl < 0.0f ? 0.0f : rl > 1.0f ? 1.0f : rl;
  float gc = gl < 0.0f ? 0.0f : gl > 1.0f ? 1.0f : gl;
  float bc = bl < 0.0f ? 0.0f : bl > 1.0f ? 1.0f : bl;
  // gamma
  float rg
    = rc <= 0.0031308f
    ? 12.92f * rc
    : 1.055f * pow(rc, 0.41666666f) - 0.055f;
  float gg
    = gc <= 0.0031308f
    ? 12.92f * gc
    : 1.055f * pow(gc, 0.41666666f) - 0.055f;
  float bg
    = bc <= 0.0031308f
    ? 12.92f * bc
    : 1.055f * pow(bc, 0.41666666f) - 0.055f;
  // output
  r = rg;
  g = gg;
  b = bg;
}

void write_png_err(png_structp png, png_const_charp msg) {
  fprintf(stderr, "error: png: %s\n", msg);
  jmp_buf *jmp = (jmp_buf *) png_get_error_ptr(png);
  if (jmp) { longjmp(*jmp, 1); } else { abort(); }
}

bool write_png(FILE *out, int width, int height, png_bytepp rows) {
  jmp_buf jmp;
  png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, &jmp, write_png_err, 0);
  if (! png) { return false; }
  png_infop info = png_create_info_struct(png);
  if (! info) { png_destroy_write_struct(&png, 0); return false; }
  if (setjmp(jmp)) { png_destroy_write_struct(&png, &info); return false; }
  png_init_io(png, out);
  png_set_compression_level(png, Z_BEST_COMPRESSION);
  png_set_IHDR
    ( png, info
    , width, height, 16
    , PNG_COLOR_TYPE_RGBA
    , PNG_INTERLACE_ADAM7
    , PNG_COMPRESSION_TYPE_DEFAULT
    , PNG_FILTER_TYPE_DEFAULT
    );
  png_set_sRGB(png, info, PNG_sRGB_INTENT_ABSOLUTE);
  png_write_info(png, info);
  png_write_image(png, rows);
  png_write_end(png, info);
  png_destroy_write_struct(&png, &info);
  return true;
}

int main(int argc, char **argv) {
  const char *stem = "chromaticity";
  if (argc > 1) { stem = argv[1]; }
  int width = 1024;
  int height = 1024;
  int depth = 100;
  // allocate image
  int bytes = width * height * 4 * sizeof(uint16_t);
  uint16_t *image = (uint16_t *) calloc(bytes, 1);
  if (! image) {
    fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
    return 1;
  }
  bytes = height * sizeof(uint16_t *);
  uint16_t **rows = (uint16_t **) calloc(bytes, 1);
  if (! rows) {
    fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
    return 1;
  }
  for (int j = 0; j < height; ++j) {
    rows[j] = image + j * width * 4;
  }
  for (int k = 0; k < depth; ++k) {
    float cyy = (k + 0.5f) / depth;
    #pragma omp parallel for schedule(static)
    for (int j = 0; j < height; ++j) {
      float cy = (height - (j + 0.5f)) / height - 0.05f;
      for (int i = 0; i < width; ++i) {
        float cx = (i + 0.5f) / width - 0.05f;
        float x = cyy * cx / cy;
        float y = cyy;
        float z = cyy * (1.0f - cx - cy) / cy;
        float rl, gl, bl;
        bool ok = xyz2srgb1(x, y, z, rl, gl, bl);
        float r = 0.0f, g = 0.0f, b = 0.0f, a = 0.0f;
        if (ok) {
          xyz2srgb2(rl, gl, bl, r, g, b, 0.0f, 1.0f);
          a = 1.0f;
        }
        uint16_t rs = fminf(fmaxf(65535.0f * r, 0), 65535);
        uint16_t gs = fminf(fmaxf(65535.0f * g, 0), 65535);
        uint16_t bs = fminf(fmaxf(65535.0f * b, 0), 65535);
        uint16_t as = fminf(fmaxf(65535.0f * a, 0), 65535);
        image[4 * (width * j + i) + 0] = htons(rs);
        image[4 * (width * j + i) + 1] = htons(gs);
        image[4 * (width * j + i) + 2] = htons(bs);
        image[4 * (width * j + i) + 3] = htons(as);
      }
    }
    // write image
    char fname[100];
    snprintf(fname, 100, "%s-%02d5.png", stem, k);
    FILE *out = fopen(fname, "wb");
    if (! out) { return 1; }
    write_png(out, width, height, (png_bytepp) rows);
    fclose(out);
  }
  return 0;
}
