#include <cmath>
#include <cstdio>
#include <cstdlib>
using namespace std;

#include "format.cc"
#include "lin2012xyz2e_1_7sf.h"

int main() {
  bool incremental = false;
  float *xyz = 0;
  float *a = 0;
  float *scanline = 0;
  int allocated_width = -1;
  int allocated_height = -1;
  int width = 0;
  int height = 0;
  float wavelength = 0;
  float white_x = 0;
  float white_y = 0;
  float white_z = 0;
  float white_a = 0;
  // for each wavelength
  int w_count = 0;
  while (read_header_intensity(stdin, &width, &height, &wavelength)) {
    fprintf(stderr, "%8d\r", w_count++);
    // allocate if necessary
    if (allocated_width < 0 && allocated_height < 0) {
      if (width > 0 && height > 0) {
        int bytes = width * height * 3 * sizeof(float);
        xyz = (float *) calloc(bytes, 1);
        if (! xyz) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        bytes = width * height * sizeof(float);
        a = (float *) calloc(bytes, 1);
        if (! a) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        bytes = width * sizeof(float);
        if (incremental) {
          bytes *= 3;
        }
        scanline = (float *) calloc(bytes, 1);
        if (! scanline) {
          fprintf(stderr, "error: memory allocation failed: %d bytes\n", bytes);
          return 1;
        }
        allocated_width = width;
        allocated_height = height;
      } else {
        fprintf(stderr, "error: bad image size: %d %d\n", width, height);
        return 1;
      }
    } // allocate if necessary
    // check image size
    if (width != allocated_width || height != allocated_height) {
      fprintf
        ( stderr
        , "error: image size mismatch: %d != %d || %d != %d\n"
        , width, allocated_width, height, allocated_height
        );
      return 1;
    }
    // compute colour
    float w  = wavelength - wavelength_min;
    int   i0 = floor(w);
    int   i1 = i0 + 1;
    float f1 = w - i0;
    float f0 = 1 - f1;
    if (f1 == 0) {
      i1 = i0;
    }
    if (! (0 <= i0 && 0 <= i1 && i1 <= wavelength_max - wavelength_min)) {
      fprintf
        ( stderr
        , "warning: skipping wavelength outside range: %f (%d, %d)\n"
        , wavelength, wavelength_min, wavelength_max
        );
      continue;
    }
    auto xyz0 = tristimulus_curve[i0];
    auto xyz1 = tristimulus_curve[i1];
    float x = f0 * xyz0[0] + f1 * xyz1[0];
    float y = f0 * xyz0[1] + f1 * xyz1[1];
    float z = f0 * xyz0[2] + f1 * xyz1[2];
    white_x += x;
    white_y += y;
    white_z += z;
    white_a += 1.0f;
    // for each scanline
    for (int j = 0; j < height; ++j) {
      read_data_intensity(stdin, width, scanline);
      #pragma omp parallel for schedule(static)
      for (int i = 0; i < width; ++i) {
        int k = j * width + i;
        float intensity = scanline[i];
        if (0.0f <= intensity && intensity < 1.0f / 0.0f) {
          xyz[3*k+0] += intensity * x;
          xyz[3*k+1] += intensity * y;
          xyz[3*k+2] += intensity * z;
          a[k] += 1.0f;
        }
      }
    } // for each scanline
    if (incremental) {
      // compute white point
      float w_x = white_x / white_a;
      float w_y = white_y / white_a;
      float w_z = white_z / white_a;
      float s = w_x + w_y + w_z;
      float cx = w_x / s;
      float cy = w_y / s;
      float cyy = w_y;
      fprintf(stderr, "info: white point: %f %f %f\n", cx, cy, cyy);
      write_header_xyz(stdout, width, height);
      // normalize image
      for (int j = 0; j < height; ++j) {
        #pragma omp parallel for schedule(static)
        for (int i = 0; i < width; ++i) {
          int k = j * width + i;
          float alpha = a[k];
          if (alpha > 0.0f) {
            scanline[3*i+0] = xyz[3*k+0] / (w_x * alpha);
            scanline[3*i+1] = xyz[3*k+1] / (w_y * alpha);
            scanline[3*i+2] = xyz[3*k+2] / (w_z * alpha);
          } else {
            scanline[3*i+0] = 0.0f;
            scanline[3*i+1] = 0.0f;
            scanline[3*i+2] = 0.0f;
          }
        }
        write_data_xyz(stdout, width, scanline);
      }
    } // if incremental
  } // for each wavelength
  if (! incremental) {
    // compute white point
    float w_x = white_x / white_a;
    float w_y = white_y / white_a;
    float w_z = white_z / white_a;
    float s = w_x + w_y + w_z;
    float cx = w_x / s;
    float cy = w_y / s;
    float cyy = w_y;
    fprintf(stderr, "info: white point: %f %f %f\n", cx, cy, cyy);
    // normalize image
    int count = width * height;
    #pragma omp parallel for schedule(static)
    for (int k = 0; k < count; ++k) {
      float alpha = a[k];
      if (alpha > 0.0f) {
        xyz[3*k+0] /= w_x * alpha;
        xyz[3*k+1] /= w_y * alpha;
        xyz[3*k+2] /= w_z * alpha;
      }
    }
    // write image
    write_header_xyz(stdout, width, height);
    write_data_xyz(stdout, count, xyz);
  }
  return 0;
}
