#ifndef SAPPHIRE_H
#define SAPPHIRE_H 1

inline float sapphire(float wavelength) {
  float l2 = 1.0e-6f * wavelength * wavelength;
  return sqrt(1.0f + l2 *
    ( 1.4313493f / (l2 - 0.0726631f*0.0726631f)
    + 0.65054713f / (l2 - 0.1193242f*0.1193242f)
    + 5.3414021f / (l2 - 18.028251f*18.028251f)
    ));
}

#endif
