#!/bin/sh
cat "${1}/database/main/SiO2/Gao.yml" |
tail -n+79 |
head -n 221 |
sed 's/\r//g' |
(
  cat <<EOF
#ifndef QUARTZ_H
#define QUARTZ_H 1

const float quartz_min   = 390;
const float quartz_step  =   2;
const int   quartz_count = 221;
const float quartz_nk[quartz_count][2] =
EOF
  sep="{"
  while read l n k
  do
    echo "  ${sep} { ${n}f, ${k}f }"
    sep=","
  done
  cat <<EOF
  };

inline float quartz(float l, int w) {
  float x  = (l - quartz_min) / quartz_step;
  int   i0 = floor(x);
  int   i1 = i0 + 1;
  float f1 = x - i0;
  float f0 = 1 - f1;
  if (i0 < 0) { return quartz_nk[0][w]; }
  if (i1 > quartz_count - 1) { return quartz_nk[quartz_count - 1][w]; }
  float y0 = quartz_nk[i0][w];
  float y1 = quartz_nk[i1][w];
  return f0 * y0 + f1 * y1;
}

#endif
EOF
)
