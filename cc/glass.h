#ifndef GLASS_H
#define GLASS_H 1

inline float glass_n(float wavelength) {
  float l2 = 1.0e-6f * wavelength * wavelength;
  return sqrt(1.0f + l2 *
    ( 1.34533359f / (l2 - 0.00997743871f)
    + 0.209073176f / (l2 - 0.0470450767f)
    + 0.937357162f / (l2 - 111.886764f)
    ));
}

const float glass_lk[13][2] =
  { { 1000.0f * 0.390f, 2.8886E-08f }
  , { 1000.0f * 0.400f, 1.9243E-08f }
  , { 1000.0f * 0.405f, 1.6869E-08f }
  , { 1000.0f * 0.420f, 1.2087E-08f }
  , { 1000.0f * 0.436f, 9.7490E-09f }
  , { 1000.0f * 0.460f, 8.8118E-09f }
  , { 1000.0f * 0.500f, 4.7818E-09f }
  , { 1000.0f * 0.546f, 3.4794E-09f }
  , { 1000.0f * 0.580f, 3.6961E-09f }
  , { 1000.0f * 0.620f, 3.9510E-09f }
  , { 1000.0f * 0.660f, 6.3120E-09f }
  , { 1000.0f * 0.700f, 4.4608E-09f }
  , { 1000.0f * 1.060f, 6.7549E-09f }
  };

inline float glass_k(float wavelength) {
  for (int i0 = 0; i0 < 12; ++i0) {
    int i1 = i0 + 1;
    float l0 = glass_lk[i0][0];
    float l1 = glass_lk[i1][0];
    if (l0 <= wavelength && wavelength <= l1) {
      float f1 = (wavelength - l0) / (l1 - l0);
      float f0 = 1.0f - f1;
      float k0 = glass_lk[i0][1];
      float k1 = glass_lk[i1][1];
      return f0 * k0 + f1 * k1;
    }
  }
  return 1.0e-6f;
}

#endif
