#ifndef WATER_H
#define WATER_H 1

const float water_min   = 375;
const float water_step  =  25;
const int   water_count =  20;
const float water_nk[water_count][2] =
  { { 1.341f, 3.50E-9f }
  , { 1.339f, 1.86E-9f }
  , { 1.338f, 1.30E-9f }
  , { 1.337f, 1.02E-9f }
  , { 1.336f, 9.35E-10f }
  , { 1.335f, 1.00E-9f }
  , { 1.334f, 1.32E-9f }
  , { 1.333f, 1.96E-9f }
  , { 1.333f, 3.60E-9f }
  , { 1.332f, 1.09E-8f }
  , { 1.332f, 1.39E-8f }
  , { 1.331f, 1.64E-8f }
  , { 1.331f, 2.23E-8f }
  , { 1.331f, 3.35E-8f }
  , { 1.330f, 9.15E-8f }
  , { 1.330f, 1.56E-7f }
  , { 1.330f, 1.48E-7f }
  , { 1.329f, 1.25E-7f }
  , { 1.329f, 1.82E-7f }
  , { 1.329f, 2.93E-7f }
  };

inline float water(float l, int w) {
  float x  = (l - water_min) / water_step;
  int   i0 = floor(x);
  int   i1 = i0 + 1;
  float f1 = x - i0;
  float f0 = 1 - f1;
  if (i0 < 0) { return water_nk[0][w]; }
  if (i1 > water_count - 1) { return water_nk[water_count - 1][w]; }
  float y0 = water_nk[i0][w];
  float y1 = water_nk[i1][w];
  return f0 * y0 + f1 * y1;
}

#endif
