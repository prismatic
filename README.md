prismatic
=========

Physics-based ray-tracing in curved space.


Usage
-----

    ./prismatic
    ./prism2xyz < prismatic-000000.prism > prismatic-000000.xyz
    ./xyz2srgb < prismatic-000000.xyz > prismatic-000000.srgb
    ./srgb2png < prismatic-000000.srgb
    display prismatic-000000.png


References
----------

Projection:
<http://paulbourke.net/geometry/transformationprojection/> section cubic to/from spherical map
<https://en.wikipedia.org/wiki/Lambert_cylindrical_equal-area_projection>
<https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates>

Ray-surface intersection distance:
<http://www.wolframalpha.com/input/?i=solve+a+%3D+b+cos%28x%29+%2B+c+sin%28x%29>
<https://en.wikipedia.org/wiki/List_of_trigonometric_identities#Double-angle_formulae>

Reflection and refraction:
<https://en.wikipedia.org/wiki/Lambertian_reflectance>
<https://en.wikipedia.org/wiki/Fresnel_equations#Power_or_intensity_equations>
<https://en.wikipedia.org/wiki/Snell%27s_law#Vector_form>
<https://www.opengl.org/sdk/docs/man4/html/reflect.xhtml>
<https://www.opengl.org/sdk/docs/man4/html/refract.xhtml>
<https://en.wikipedia.org/wiki/Kramers%E2%80%93Kronig_relations>

Absorption and Emission:
<https://en.wikipedia.org/wiki/Beer%E2%80%93Lambert_law>
<http://en.wikipedia.org/wiki/Refractive_index#Complex_refractive_index>
<https://en.wikipedia.org/wiki/Absorption_spectroscopy>
<https://en.wikipedia.org/wiki/Emission_spectrum>

Colour, wavelength, CIE XYZ, sRGB:
<http://cvrl.ioo.ucl.ac.uk> section database / CMFs
<https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation>
<https://en.wikipedia.org/wiki/Illuminant_D65>
<https://en.wikipedia.org/wiki/CMYK_color_model>

Materials:
<http://refractiveindex.info/>
<http://en.wikipedia.org/wiki/Corundum>
<http://en.wikipedia.org/wiki/Sapphire>
<http://hypertextbook.com/facts/2007/GaryChang.shtml>
<http://www.jewelinfo4u.com/Ruby_Identification.aspx>
<http://webbook.nist.gov/chemistry/>
<http://webbook.nist.gov/chemistry/uv-vis/>
<https://en.wikipedia.org/wiki/Crown_glass_%28optics%29>
<https://en.wikipedia.org/wiki/Flint_glass>


Benchmarks
----------

Fixed size (4), varying depth:

    real      user      sys
1 0m01.911s 0m06.948s 0m0.016s
2 0m05.534s 0m20.909s 0m0.012s
3 0m12.855s 0m48.483s 0m0.028s
4 0m26.562s 1m42.266s 0m0.040s
5 0m54.689s 3m30.053s 0m0.116s
6 1m51.469s 7m02.346s 0m0.172s

Fixed depth (3), varying size:

1 0m00.294s 00m00.980s 0m0.008s
2 0m00.806s 00m03.096s 0m0.004s
3 0m03.333s 00m12.277s 0m0.024s
4 0m12.893s 00m48.647s 0m0.020s
5 0m50.431s 03m12.780s 0m0.076s
6 3m18.849s 12m46.232s 0m0.356s


Optimisations
-------------

Phase 1:

baseline port from GLSL

real    0m43.529s
user    0m43.519s
sys     0m0.000s

early escape, less trig

real    0m33.588s
user    0m33.478s
sys     0m0.080s

tangent transport

real    0m26.727s
user    0m26.718s
sys     0m0.004s

Phase 2:

baseline

real    1m1.375s
user    2m15.520s
sys     0m0.028s

omp schedule pragmas

real    0m33.954s
user    2m14.184s
sys     0m0.028s
