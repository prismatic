#donotrun

#buffer RGBA32F
#buffershader "prismatic-buffer.frag"

#vertex

#group Camera
uniform vec2 Center; slider[(-10,-10),(0,0),(10,10)] NotLockable
uniform float Zoom; slider[0,1,2] NotLockable

uniform vec2 pixelSize;

varying vec2 coord;
varying vec2 viewCoord;

void main() {
  float ar = pixelSize.y/pixelSize.x;
  gl_Position = gl_Vertex;
  viewCoord = gl_Vertex.xy;
  coord = ((gl_ProjectionMatrix*gl_Vertex).xy * vec2(ar, 1.0)) / Zoom + Center;
}

#endvertex

varying vec2 coord;
varying vec2 viewCoord;

// forward declarations
void init();
vec3 color(vec2 z);

uniform sampler2D backbuffer;

void main() {
  init();
  vec4 prev = texture2D(backbuffer, (viewCoord + vec2(1.0)) / 2.0);
  vec4 new = vec4(color(coord.xy), 1.0);
  if (new != new) { new = vec4(0.0); } // NaN check
  gl_FragColor = prev + vec4(new.rgb * new.a, new.a);
}
