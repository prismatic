#version 330 compatibility

#include "prismatic-2d.frag"

uniform int subframe;
uniform float time;

const float pi = 3.141592653589793;


#group Animation

uniform float Duration; slider[1,15,300]
uniform int Spin; slider[-15,-1,15]
uniform int Push; slider[-15,2,15]

#group Rotation

uniform float XY; slider[-180,0,180]
uniform float XZ; slider[-180,30,180]
uniform float XW; slider[-180,45,180]
uniform float YZ; slider[-180,-15,180]
uniform float YW; slider[-180,60,180]
uniform float ZW; slider[-180,30,180]

#group RayTrace

// XYZ tristimulus response curves
uniform sampler2D Spectrum; file[xyz_390nm-830nm.png]
const float mincolour = 0.39; // violet
const float maxcolour = 0.83; // red

uniform float Gain; slider[-10,0,10]

uniform int Depth; slider[1,3,6]
uniform int Count; slider[1,10,500]

uniform float Sphere; slider[0,0.66666,1]
uniform float Light; slider[0,0.1,1]


#group Material

uniform vec3 TransmissionWavelength; slider[(0.39,0.39,0.39),(0.45,0.58,0.65),(0.83,0.83,0.83)]
uniform vec3 TransmissionQuality; slider[(0.0,0.0,0.0),(8.0,8.0,8.0),(64.0,64.0,64.0)]
uniform vec3 TransmissionAttenuation; slider[(0.0,0.0,0.0),(3.0,3.0,3.0),(16.0,16.0,16.0)]
uniform vec3 DiffuseWavelength; slider[(0.39,0.39,0.39),(0.45,0.58,0.65),(0.83,0.83,0.83)]
uniform vec3 DiffuseQuality; slider[(0.0,0.0,0.0),(4.0,4.0,4.0),(64.0,64.0,64.0)]
uniform vec3 DiffuseIntensity; slider[(0.0,0.0,0.0),(1.0,1.0,1.0),(10.0,10.0,10.0)]

// wavelength of current subframe
float colourTheta = 0.75 * (float(subframe % Count) + 0.5) / float(Count);
float colour = mix(mincolour, maxcolour, colourTheta);

// corresponding XYZ
vec3 basecolour = texture(Spectrum, vec2(colourTheta, 0.5)).xyz;

float refractionIndex = 2.5 - colourTheta;

// trace rays this deep
const int ndepth = 6;
const int ncontext = 1 << (1 + ndepth);

// advance rays away from surface intersection
const float acne = 0.001;


// spheres at the vertices of a 24-cell with lights at their centers
const int nspheres = 24;
const int nlights = nspheres;
struct sphere {
  vec4  center;
  float sradius; // sin(radius)
  float index;   // refractive index
  int   colour;  // wavelength in micrometers, or 0 for white
};
sphere scene[nspheres + nlights];


// no recursion in shaders, so emulate it with a queue
struct context {
  vec4  eye;       // ray start point
  vec4  ray;       // ray direction vector
  float factor;    // contribution of this ray to the total
  int   material;  // what the ray is travelling through
  int   depth;     // pseudo-recursion depth
};
context queue[ncontext];
int qread  = 0;
int qwrite = 0;

// check if the queue is empty
bool empty() { return !(qread < qwrite); }

// append to the queue
void enqueue(vec4 eye, vec4 ray, float factor, int material, int depth) {
  if (depth < Depth && depth < ndepth && qwrite < ncontext) {
    queue[qwrite].eye = eye;
    queue[qwrite].ray = ray;
    queue[qwrite].factor = factor;
    queue[qwrite].material= material;
    queue[qwrite].depth = depth;
    qwrite = qwrite + 1;
  }
}


float attenuation(int material) {
  float c = 0.0;
  switch (material) { // work around NVIDIA driver bug where dynamic indexing takes forever to link
    // zero at material colour (perfect transmission), higher elsewhere
    case 0:
      c = (TransmissionWavelength[0] - colour) / (maxcolour - mincolour);
      return TransmissionAttenuation[0] * (1.0 - 1.0 / (1.0 + TransmissionQuality[0] * TransmissionQuality[0] * c * c));
    case 1:
      c = (TransmissionWavelength[1] - colour) / (maxcolour - mincolour);
      return TransmissionAttenuation[1] * (1.0 - 1.0 / (1.0 + TransmissionQuality[1] * TransmissionQuality[1] * c * c));
    case 2:
      c = (TransmissionWavelength[2] - colour) / (maxcolour - mincolour);
      return TransmissionAttenuation[2] * (1.0 - 1.0 / (1.0 + TransmissionQuality[2] * TransmissionQuality[2] * c * c));
    // empty space doesn't attenuate
    default:
      return 0.0;
  }
}


float emission(int material) {
  float c = 0.0;
  switch (material) { // work around NVIDIA driver bug where dynamic indexing takes forever to link
    // peak at material colour, lower elsewhere
    case 0:
      c = (DiffuseWavelength[0] - colour) / (maxcolour - mincolour);
      return DiffuseIntensity[0] / (1.0 + DiffuseQuality[0] * DiffuseQuality[0] * c * c);
    case 1:
      c = (DiffuseWavelength[1] - colour) / (maxcolour - mincolour);
      return DiffuseIntensity[1] / (1.0 + DiffuseQuality[1] * DiffuseQuality[1] * c * c);
    case 2:
      c = (DiffuseWavelength[2] - colour) / (maxcolour - mincolour);
      return DiffuseIntensity[2] / (1.0 + DiffuseQuality[2] * DiffuseQuality[2] * c * c);
    default:
      // lights are bright
      return 16.0;
  }
}


// spherical tangent space
// points and directions are unit length
// directions from the pole are equivalent to points on the equator
// result is direction towards "to" orthogonal to "from"
vec4 tangent(vec4 to, vec4 from) {
  return normalize(to - dot(to, from) * from);
}

// Beer-Lambert law
float transmit(float attenuation, float pathlength) {
  return exp(-attenuation * pathlength);
}

// Fresnel equations
// I = incident ray
// N = surface normal
// eta = n1 / n2
// result.x = reflected power
// result.y = transmitted power
vec2 fresnel(vec4 I, vec4 N, float eta) {
  float c1 = -dot(I, N);
  float c2 = 1.0 - (1.0 - c1 * c1) * (eta * eta);
  if (c2 < 0.0) {
    // total internal reflection
    return vec2(1.0, 0.0);
  }
  c2 = sqrt(c2);
  float Rs = (eta * c1 - c2) / (eta * c1 + c2);
  float Rp = (eta * c2 - c1) / (eta * c2 + c1);
  float R = 0.5 * (Rs * Rs + Rp * Rp);
  float T = 1.0 - R;
  return vec2(R, T);
}


// trace rays
float trace() {
  // accumulated from all rays
  float intensity = 0.0;
  // while there are rays in the queue
  for (int count = 0; count < ncontext; ++count) {
    if (empty()) { break; }

    // dequeue a ray
    vec4  eye      = queue[qread].eye;
    vec4  ray      = queue[qread].ray;
    float factor   = queue[qread].factor;
    int   material = queue[qread].material;
    int   depth    = queue[qread].depth;
    qread = qread + 1;
    // initialize results
    bool  hit         = false;      // did the ray hit anything
    bool  hitlight    = false;      // if so, did it hit a light
    float hitdistance = 1.0 / 0.0;  // keep only the nearest hit
    vec4  hitpos      = vec4(0.0);  // ray-surface intersection point
    vec4  hitnormal   = vec4(0.0);  // surface normal at intersection
    int   hitmaterial = -1;        // material beyond the surface
    float eta         = 1.0;        // ratio of refraction indices
    float emit        = 0.0;        // emission from surface

    // check every object in the scene
    for (int k = 0; k < nspheres + nlights; ++k) {
      vec4  center  = scene[k].center;
      // r = sphere radius
      float sr      = scene[k].sradius;
      float cr      = sqrt(1.0 - sr * sr);
      // d = distance from eye to center
      float cd      = dot(eye, center);
      float sd      = sqrt(1.0 - cd * cd);
      // theta = angle subtended by the sphere radius
      float stheta  = sr / sd;
      // phi = angle between ray and sphere center
      vec4  tcenter = tangent(center, eye);
      float cphi    = dot(ray, tcenter);

      if (stheta > 1.0) {
        // d < r
        // sphere surrounds eye, ray intersects from the inside
        // ignore lights
        if (k < nspheres) {
          // find the distance to the intersection
          // ... c + sqrt ... because the other intersection is behind
          float a = cr;
          float b = cd;
          float c = sd * cphi;
          float h = 2.0 * atan(c + sqrt(c * c + b * b - a * a), a + b);
          // check if it's closer
          if (0.0 < h && h < hitdistance) {
            // psi = angle between ray and surface normal
            float sphi = sqrt(1.0 - cphi * cphi);
            float spsi = sphi / stheta;
            float cpsi = sqrt(1.0 - spsi * spsi);
            // update results with new details
            hit         = true;
            hitlight    = false;
            hitdistance = h;
            // eye and ray are orthogonal unit vectors
            hitpos      = cos(h) * eye + sin(h) * ray;
            // surface normal points towards sphere center
            hitnormal   = tangent(center, hitpos);
            // outside the sphere is vacuum
            hitmaterial = -1;
            eta         = scene[k].index / 1.0;
            // Lambertian diffuse reflection
            emit        = emission(scene[k].colour) * clamp(cpsi, 0.0, 1.0);
          }
        }

      } else {
        // d > r
        // sphere is disjoint from eye, ray might intersect from outside
        float ctheta = sqrt(1.0 - stheta * stheta);
        if (cphi > ctheta) {
          // phi < theta
          // ray does intersect
          // find the distance to the intersection
          // ... c - sqrt ... because the other intersection is further
          float a = cr;
          float b = cd;
          float c = sd * cphi;
          float h = 2.0 * atan(c - sqrt(c * c + b * b - a * a), a + b);
          // check if it's closer
          if (0.0 < h && h < hitdistance) {
            // psi = angle between ray and surface normal
            float sphi = sqrt(1.0 - cphi * cphi);
            float spsi = sphi / stheta;
            float cpsi = sqrt(1.0 - spsi * spsi);
            // update results with new details
            hit         = true;
            hitlight    = nspheres <= k;
            hitdistance = h;
            // eye and ray are orthogonal unit vectors
            hitpos      = cos(h) * eye + sin(h) * ray;
            // surface normal points away from sphere center
            hitnormal   = -tangent(center, hitpos);
            // inside the sphere is the sphere's material
            hitmaterial = scene[k].colour;
            // outside the sphere is vacuum
            eta         = 1.0 / scene[k].index;
            // Lambertian diffuse reflection
            emit        = emission(scene[k].colour) * clamp(cpsi, 0.0, 1.0);
          }
        }
      }
    } // for each object in scene

    if (hit) {
      // attenuation by the near-side material through which the ray travelled
      float transmission = transmit(attenuation(material), hitdistance);
      // non-physical fading with distance
      float fade = 0.5 * (1.0 + cos(hitdistance));
      // contribution from this ray
      float factor2 = factor * transmission * fade;
      // accumulate this ray
      intensity += factor2 * emit;
      if (! hitlight) {
        // incident ray
        vec4 incident = tangent(ray, hitpos);
        // parallel transport past the equator
        if (hitdistance > pi / 2.0) { incident = -incident; }
        // Fresnel equations for intensity of reflection and refraction
        vec2 f = fresnel(incident, hitnormal, eta);
        if (f.x > 0.0) {
          // reflected ray travels through the near side material
          vec4 ray2 = tangent(normalize(reflect(incident, hitnormal)), hitpos);
          vec4 eye2 = normalize(hitpos + acne * ray2);
          ray2 = tangent(ray2, eye2);
          enqueue(eye2, ray2, factor2 * f.x, material, depth + 1);
        }
        if (f.y > 0.0) {
          // refracted ray travels through the far side material
          vec4 ray2 = tangent(normalize(refract(incident, hitnormal, eta)), hitpos);
          vec4 eye2 = normalize(hitpos + acne * ray2);
          ray2 = tangent(ray2, eye2);
          enqueue(eye2, ray2, factor2 * f.y, hitmaterial, depth + 1);
        }
      }
    }

  } // while there are rays in the queue
  return intensity;
}


// base rotation transform
float cXY = cos(radians(XY));
float sXY = sin(radians(XY));
float cXZ = cos(radians(XZ));
float sXZ = sin(radians(XZ));
float cXW = cos(radians(XW));
float sXW = sin(radians(XW));
float cYZ = cos(radians(YZ));
float sYZ = sin(radians(YZ));
float cYW = cos(radians(YW));
float sYW = sin(radians(YW));
float cZW = cos(radians(ZW));
float sZW = sin(radians(ZW));
mat4 mXYZW = mat4
  ( cXY, sXY, 0.0, 0.0
  , -sXY, cXY, 0.0, 0.0
  , 0.0, 0.0, cZW, sZW
  , 0.0, 0.0, -sZW, cZW
  );
mat4 mXZYW = mat4
  ( cXZ, 0.0, sXZ, 0.0
  , 0.0, cYW, 0.0, sYW
  , -sXZ, 0.0, cXZ, 0.0
  , 0.0, -sYW, 0.0, cYW
  );
mat4 mXWYZ = mat4
  ( cXW, 0.0, 0.0, sXW
  , 0.0, cYZ, sYZ, 0.0
  , 0.0, -sYZ, cYZ, 0.0
  , -sXW, 0.0, 0.0, cXW
  );
mat4 m0 = mXYZW * mXZYW * mXWYZ;

// animation rotation transform
float timeR = 2.0 * pi * time / Duration;
float cSpin = cos(float(Spin) * timeR);
float sSpin = sin(float(Spin) * timeR);
float cPush = cos(float(Push) * timeR);
float sPush = sin(float(Push) * timeR);
mat4 m1= mat4
  ( cSpin, sSpin, 0.0, 0.0
  , -sSpin, cSpin, 0.0, 0.0
  , 0.0, 0.0, cPush, sPush
  , 0.0, 0.0, -sPush, cPush
  );

// combined transform
mat4 m = m1 * m0;



// initialize
void init() {
  // initialize scene
  // sphere radius
  float sr = sin(Sphere * pi / 6.0);
  // light radius
  float lr = sin(Light * Sphere * pi / 6.0);
  // non-physical index of refraction dependent on wavelength
  float idx = refractionIndex;
  // 24-cell has the vertices of a cube and a cross
  // three sets of eight vertices, each set in its own colour
  int k = 0;
  // cube
  for (int sx = -1; sx <= 1; sx += 2) {
  for (int sy = -1; sy <= 1; sy += 2) {
  for (int sz = -1; sz <= 1; sz += 2) {
  for (int sw = -1; sw <= 1; sw += 2) {
    bool odd = ((sx + sy + sz + sw) & 2) == 2;
    vec4 p = m * (0.5 * vec4(float(sx), float(sy), float(sz), float(sw)));
    scene[k].center  = p;
    scene[k].sradius = sr;
    scene[k].index   = idx;
    scene[k].colour  = odd ? 0 : 1;
    scene[k+nspheres].center  = p;
    scene[k+nspheres].sradius = lr;
    scene[k+nspheres].index   = 1.0;
    scene[k+nspheres].colour  = -1;
    k = k + 1;
  }}}}
  // cross
  for (int sd = 0; sd < 4; sd += 1) {
  for (int sv = -1; sv <= 1; sv += 2) {
    vec4 p = vec4(0.0);
    p[sd] = float(sv);
    p = m * p;
    scene[k].center  = p;
    scene[k].sradius = sr;
    scene[k].index   = idx;
    scene[k].colour  = 2;
    scene[k+nspheres].center  = p;
    scene[k+nspheres].sradius = lr;
    scene[k+nspheres].index   = 1.0;
    scene[k+nspheres].colour  = -1;
    k = k + 1;
  }}
}


// compute the colour for pixel
vec3 color(vec2 screen) {
  // https://en.wikipedia.org/wiki/Lambert_cylindrical_equal-area_projection
  if (abs(screen.y) >= 1.0) { return vec3(0.0); }
  vec4 eye = vec4(0.0, 0.0, 0.0, 1.0);
  vec4 ray = normalize(vec4
    ( sqrt(1.0 - screen.y * screen.y) * sin(screen.x)
    , screen.y
    , sqrt(1.0 - screen.y * screen.y) * cos(screen.x)
    , 0.0
    ));

  // compute the material at the eye
  float neardist= -2.0;
  int neark = 0;
  for (int k = 0; k < nspheres; ++k) {
    float d = dot(eye, scene[k].center);
    if (d > neardist) {
      neardist = d;
      neark = k;
    }
  }
  int material = -1;
  float sr = scene[neark].sradius;
  if (neardist > sqrt(1.0 - sr * sr)) {
    material = scene[neark].colour;
  }

  // compute the resulting CIE XYZ colour
  enqueue(eye, ray, 1.0, material, 0);
  float intensity = trace();
  return intensity * basecolour;
}
