#donotrun

#vertex

varying vec2 coord;

void main() {
  gl_Position = gl_Vertex;
  coord = (gl_ProjectionMatrix * gl_Vertex).xy;
}

#endvertex

varying vec2 coord;

uniform sampler2D frontbuffer;

uniform float Gain;

float srgb(float c0) {
  float c = clamp(c0, 0.0, 1.0);
  if (c <= 0.0031308) {
    return 12.92 * c;
  } else {
    return 1.055 * pow(c, 1.0 / 2.4) - 0.055;
  }
}

vec3 srgb(vec3 xyz) {
  vec3 rgb = xyz * mat3
    ( 3.2406, -1.5372, -0.4986
    , -0.9689, 1.8758, 0.0415
    , 0.0557, -0.2040, 1.0570
    );
  return vec3(srgb(rgb.r), srgb(rgb.g), srgb(rgb.b));
}

void main() {
  vec2 pos = (coord+vec2(1.0))/2.0;
  vec4 tex = texture2D(frontbuffer, pos);
  vec3 xyz = exp2(Gain) * tex.xyz / tex.a;
  gl_FragColor = vec4(srgb(xyz), 1.0);
}
